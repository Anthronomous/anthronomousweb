/*
	Anthronomous Main
	Description: Script that runs on page load and does most things for site operation.
*/

window.self.onload = function(){
	'use strict';
	
	// Set navigation buttons to active for the current page and stuff
	(function() {
		var pageData = (function() {
			var data = {
				activeNavButtonClass: 'nav-button-active',
				pages: {
					notices: {key: 'notices', headerText: 'Notices'},
					journal: {key: 'journal', headerText: 'Journal'},
					data:    {key: 'data', headerText: 'Data'},
					results: {key: 'results', headerText: 'Results'},
					about:   {key: 'about', headerText: 'About'},
					project: {key: 'project', headerText: 'Project'},
					author:  {key: 'author', headerText: 'Author'}
				}
			};
			
			// The level of nesting for a given page (like a directory path)
			data.nesting = {
				notices: [data.pages.notices],
				journal: [data.pages.journal],
				data:    [data.pages.data],
				results: [data.pages.data, data.pages.results],
				about:   [data.pages.about],
				project: [data.pages.about, data.pages.project],
				author:  [data.pages.about, data.pages.author]
			};
			
			return data;
		}());
		
		var page = document.body.getAttribute('data-page');
		if (page === null || page === '' || !Object.prototype.hasOwnProperty.call(pageData.pages, page))
			return;
		
		var page_nesting = pageData.nesting[page];
		var button_keys = [];
		var i;
		for(i=0; i<page_nesting.length; i++){
			button_keys.push(page_nesting[i].key);
		}
		var nav_buttons = document.querySelectorAll('nav a.nav-button[data-button="' + button_keys.join('"], nav a.nav-button[data-button="') + '"]');
		for (i=0; i<nav_buttons.length; i++)
			nav_buttons.item(i).classList.add(pageData.activeNavButtonClass);
	}());
	
	// Syntax highlighting
	(function() {
		var code_blocks = document.querySelectorAll('pre > code');
		var i;
		for (i=0; i<code_blocks.length; i++) {
			// Remove white space before the code starts
			var element = code_blocks.item(i);
			element.innerText = element.innerText.replace(/^\s+/, '');
			hljs.highlightBlock(element);
		}
	}());
	
	// Navigation drop down menus
	(function() {
		var ESCAPE_KEY_CODE = 27;
		
		var dropdown = {
			cssFocusClass: 'dropdown-menu-focus',
			setupEvents: function() {
				var dropdown_menus = document.querySelectorAll('nav > ul.top-nav-dropdown-container > li > div.top-nav-dropdown-menu');
				var i, j;
				for (i=0; i<dropdown_menus.length; i++){
					// Need to handle focus for the label too
					dropdown_menus.item(i).previousElementSibling.onfocus = dropdown.onLabelFocus;
					
					var dropdown_links = dropdown_menus.item(i).querySelectorAll('a');
					for (j=0; j<dropdown_links.length; j++) {
						var link = dropdown_links.item(j);
						link.parentMenu = dropdown_menus.item(i);
						link.onfocus = this.onLinkFocus;
					}
				}
			},
			
			onLinkFocus: function(e) {
				this.onblur = dropdown.onLinkBlur;
				this.onkeyup = dropdown.onKeyUp;
				
				// Only add the class when neccessary
				var prev_parent_menu_type = null;
				if (e.relatedTarget !== null)
					prev_parent_menu_type = typeof e.relatedTarget.parentMenu;
				
				if (prev_parent_menu_type !== 'object' || (prev_parent_menu_type === 'object' && this.parentMenu !== e.relatedTarget.parentMenu)) {
					this.parentMenu.classList.add(dropdown.cssFocusClass);
					this.parentMenu.previousElementSibling.style.marginBottom = '0.2rem';		// .dropdown-label
				}
			},
			
			onLinkBlur: function(e) {
				dropdown.removeFocusedEventListeners(this);
				
				// Different drop down menu
				if (e.relatedTarget !== null && typeof e.relatedTarget.parentMenu === 'object') {
					if (e.relatedTarget.parentMenu !== this.parentMenu) {
						this.parentMenu.classList.remove(dropdown.cssFocusClass);
						this.parentMenu.previousElementSibling.style.marginBottom = '';		// Restore old value for .dropdown-label's bottom margin
					}
				}
				// No drop down menu
				else {
					this.parentMenu.classList.remove(dropdown.cssFocusClass);
					this.parentMenu.previousElementSibling.style.marginBottom = '';
				}
			},
						
			onKeyUp: function(e) {
				if (e.keyCode === ESCAPE_KEY_CODE) {
					this.blur();
				}
			},
			
			onLabelFocus: function(e) {
					this.onkeyup = dropdown.onKeyUp;
					this.onblur = dropdown.onLabelBlur;
			},
			
			onLabelBlur: function(e) {
				dropdown.removeFocusedEventListeners(this);
			},
			
			removeFocusedEventListeners: function(element) {
				element.onblur = null;
				element.onkeyup = null;
			}
		};
		
		dropdown.setupEvents();
	}());
};